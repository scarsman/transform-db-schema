#import pytest
import os
from termcolor import colored
from transform_db_schema import transform, compare_lookup_row_data, execute_statement


db_filename_old = os.path.join(os.getcwd(), "PeerDB_G.db")
db_filename_new = os.path.join(os.getcwd(), "PeerDB.db") 


def test_dbs_schemas_are_equal():
	
	print("\n==> TEST TWO DB'S SCHEMA ARE EQUAL")
	
	schemas_status, indexes_status = transform(db_filename_old, db_filename_new)
	
	print("\t 2 DBS SCHEMAS ARE %s " % colored(schemas_status,"green"))

def test_dbs_indexes_are_equal():
	
	print("\n==> TEST TWO DB'S INDEXES ARE EQUAL")
	
	schemas_status, indexes_status = transform(db_filename_old, db_filename_new)
	
	print("\t 2 DBS INDEXES ARE %s " % colored(indexes_status,"green"))	
	
	
def test_tables_rows_data_are_equal():
	
	print("\n==> TEST TABLES ROWS DATA ARE EQUAL BETWEEN TWO DB'S")
	
	stmt = "SELECT name FROM sqlite_master WHERE type='table' and name != 'sqlite_sequence' and name like 'L_%';"
	
	tables_old = execute_statement(stmt, db_filename_old, get_results=True)
	tables_new =  execute_statement(stmt, db_filename_new, get_results=True)
	
	
	tables_old = sorted(list(zip(*tables_old))[0])
	tables_new = sorted(list(zip(*tables_new))[0])
	
	print("\t old db lookup table --> %s" % tables_old)
	print("\t new db lookup table --> %s" % tables_new)
	print("\n")
	
	counter = 0
	
	if len(tables_old) != len(tables_new):
		print("\t The return  tables list length are not equal")
		
		return
	
	while counter < len(tables_old) and counter < len(tables_new):
		if tables_old[counter] != tables_new[counter]:
			
			print("\t The return  tables list indexes data are not equal")
			
			break
		
		result = compare_lookup_row_data(db_filename_old, tables_old[counter], db_filename_new, tables_new[counter])
		
		print("\t %s -> %s ` %s ` TO %s -> %s " %(db_filename_old, tables_old[counter], colored(result, "green"), db_filename_new,  tables_new[counter]))
		
		counter = counter + 1


if __name__ == "__main__":
	test_dbs_schemas_are_equal()
	test_dbs_indexes_are_equal()
	test_tables_rows_data_are_equal()
	