import sqlite3
import json
import re
import logging
import sys

from argparse import ArgumentParser


LOGGING_LEVEL = logging.INFO

logging.basicConfig(filename="transformdb.log",level=LOGGING_LEVEL, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler(sys.stdout))


def db_structure(db_filepath):

	stmt = "SELECT name FROM sqlite_master WHERE type='table' and name != 'sqlite_sequence';"
	tables = execute_statement(stmt, db_filepath, get_results=True)
	table_names = sorted(list(zip(*tables))[0])
	
	log.debug("--Tables for database %s \n" % db_filepath)
	log.debug("\n".join(table_names))
	log.debug("\n")
	
	results = {}
	table_data = {}
	
	sql_data_types = [
		"NOT NULL", "DATE", "SMALLINT", "PRIMARY KEY", "FOREIGN KEY", "AUTOINCREMENT",
		"NULL", "INTEGER", "CHAR", "VARCHAR", "REFERENCES", "DEFAULT", "CURRENT_TIMESTAMP",
		"not null", "smallint", "foreign key", "autoincrement","references",
		"integer", "primary key", "char", "varchar", "current_timestamp"
	]
	
	for table_name in table_names:
		columns_data = {}
		columns_schema = {}
		
		log.debug("-- EXTRACTING TABLE %s" % table_name)
		
		#get table columns via pragma_table_info
		stmt = "select * from pragma_table_info('%s');" % table_name
		result_pragma_table_info = execute_statement(stmt, db_filepath, get_results=True)
	
		for column_id, column_name, column_type, column_not_null, column_default, column_pk in result_pragma_table_info:

			id = column_id
			name = column_name
			type = column_type
			null = ""
			default= ""
			pk = ""
			
			if column_not_null:
				null = " NOT NULL"
			
			if column_default:
				default = "%s" % column_default
			
			if column_pk:
				pk = " *%s" % column_pk
			
			
			col_name = name
			col_schema =  "%s %s %s %s" %(type, null, default, pk)
			
			
			#replace small data types text to uppercase
			for tag in sql_data_types:
				#if tag in col_schema:
				#	log.debug(tag)
				col_schema = col_schema.replace(tag, tag.upper())
			
			log.debug(col_name + "--> "+ col_schema)
			
			columns_schema[col_name] = col_schema
				
		columns_data["columns"] = columns_schema
		
		#get the original create table statement, useful for adding new table
		stmt = "SELECT sql FROM sqlite_master WHERE type='table' and name='%s';" % table_name
		original = execute_statement(stmt, db_filepath, get_results=True)
		
		for sql_stmt in original:
			columns_data["sql_stmt"] = " ".join(sql_stmt).replace("[","").replace("]","")
		
		#get tables foreign keys
		sql_pragma_foreign_list = """SELECT 
			m.name,
			p."table",
			p."from",
			p."to"
			FROM
			sqlite_master m
			JOIN pragma_foreign_key_list(m.name) p ON m.name != p."table"
			WHERE m.type = 'table' and m.name="%s"
			ORDER BY m.name;
		""" % table_name
		
		res = execute_statement(sql_pragma_foreign_list, db_filepath, get_results=True)
		
		foreign_keys = []
		
		#log.debug("- FOREIGN KEYS")
		
		for table,reference_table,table_from,table_refer_to in res:
			#log.debug(table +"->"+ reference_table +" "+ table_from +" "+ table_refer_to)
			foreign_keys.append(reference_table +" "+ table_from +" "+ table_refer_to)
			
		columns_data["foreign_keys"] = foreign_keys
		
		
		table_data[table_name] = columns_data
	
	stmt = "SELECT name,sql FROM sqlite_master WHERE type='index' and name not like 'sqlite_autoindex%';"
	indexes = execute_statement(stmt, db_filepath, get_results=True)

	index_data = {}
	for key, val in indexes:
		value = " ".join(val.split()).replace("[","").replace("]","")
		#remove  whitespace between special characters and words/letters
		value = re.sub("\s*(\W)\s*",r"\1", value)
		
		index_data[key] = value

	results["TABLES"] = table_data
	results["INDEXES"] = index_data


	return results

def compare_results(old_contents, new_contents):
	
	old_contents_keys = set(old_contents.keys())
	new_contents_keys = set(new_contents.keys())
	intersect_keys = old_contents_keys.intersection(new_contents_keys)
	
	new = new_contents_keys - old_contents_keys
	new = list(new)
	
	deleted = old_contents_keys - new_contents_keys
	deleted = list(deleted)
		
	modified = {data : {"old_print": old_contents[data], "new_print": new_contents[data]} for data in intersect_keys if old_contents[data] != new_contents[data]}
	
	same = set(data for data in intersect_keys if old_contents[data] == new_contents[data])
	
	same = list(same)
	
	return new, deleted, modified, same

	
def execute_statement(sql_statement, db_to_transform, get_results=False, multiple_statement=False):
	db=sqlite3.connect(db_to_transform)
	db.text_factory = str
	cur = db.cursor()
	
	results = None

	if multiple_statement:
		results = cur.executescript(sql_statement)
	else:
		if get_results:
			results = cur.execute(sql_statement).fetchall()
		else:			
			results = cur.execute(sql_statement)	
			
	db.close()
	
	return results
	
def process_new_table(_results, new_table_results, db_to_transform,  new_db):
	if _results:
		
		log.debug("- Process new tables")
		
		for new_table in _results:
			log.debug("add new table `%s`" % new_table)
			sql_stmt = new_table_results[new_table]["sql_stmt"]
			
			log.debug("sql statement %s" % sql_stmt)
			execute_statement(sql_stmt, db_to_transform)
			
			#if lookup table replicate the row data from the new db to the transform db
			if "L_" in new_table:
				#add new data here from new db to this transform db'
				stmt = """
				ATTACH DATABASE '%s' AS old_db;
				ATTACH DATABASE '%s' AS new_db;
				INSERT INTO old_db.%s SELECT * FROM new_db.%s;
				""" %(db_to_transform, new_db, new_table, new_table)
				
				log.debug("sql statement %s" % stmt)
				
				
				execute_statement(stmt, db_to_transform, multiple_statement=True)
			
			
			
def process_deleted_table(_results, db_to_transform):
	if _results:
		
			log.debug("- Process deleted tables")
			
			for delete_table in _results:
				sql_stmt = "DROP TABLE %s;" % delete_table
				log.debug("Deleting table %s " % delete_table)
				log.debug("Sql Statement %s " % sql_stmt)
				execute_statement(sql_stmt, db_to_transform)

def process_modified_table(_results, new_table_results, db_to_transform):
	new_print = None
	old_print = None
	
	if _results:
		
		log.debug("- Process modified tables")
		
		log.debug(json.dumps(_results, indent=4))
		
		for table,val in _results.items():
			old_print = _results[table]["old_print"]["columns"]
			new_print = _results[table]["new_print"]["columns"]
			
			new_fields, deleted_fields, modified_fields, same_fields = compare_results(old_print, new_print)
			
			
			log.debug("-- new field names for table `%s`" % table)
			log.debug(json.dumps(new_fields, indent=2))
			
			log.debug("-- deleted field names for table `%s`" % table)
			log.debug(json.dumps(deleted_fields, indent=2))
			
			log.debug("-- modified field names for table `%s`" % table)
			log.debug(json.dumps(modified_fields, indent=2))
			
			#table add new fields names
			if new_fields:
				for field_name in new_fields:
					
					log.debug("%s : %s" % (field_name, new_print[field_name]))
					
					#retract to sql_stmt, get the original fieldname schema
					
					sql_stmt =  _results[table]["new_print"]["sql_stmt"]
					
					split_stmt = sql_stmt.split("\n")
					
					if "\r\n" in sql_stmt:
						split_stmt = sql_stmt.split("\r\n")

					fieldname_schema = None
					#get the original fieldname schema
					for index, j in enumerate(split_stmt):
						if field_name in j:
							fieldname_schema = split_stmt[index] 
							
					clean_fieldname = fieldname_schema.strip()[:-1]
					
					
					#sqlite doesnt support altering date if default to timestamp
					if "CURRENT_TIMESTAMP" in clean_fieldname:

						#delete the new fieldname/s because old data don't have this fieldname
						#del new_print[field_name]
						
						new_print_clean = list(set(new_print.keys()) - set(new_fields))
						
						#drop table if exist
						drop_table = "DROP TABLE IF EXISTS %s_old;" % table
						execute_statement(drop_table, db_to_transform)
						#rename table
						rename_table = "ALTER TABLE %s RENAME TO %s_old;" %(table, table)
						execute_statement(rename_table, db_to_transform)
						
						#re-create table with new schema
						sql_stmt = new_table_results[table]["sql_stmt"]
						
						execute_statement(sql_stmt, db_to_transform)
						
						#reinsert data to the new table
						new_fieldnames = ", ".join(new_print_clean)
						insert_statement = "INSERT INTO %s (%s) SELECT %s FROM %s_old;" % (table, new_fieldnames, new_fieldnames, table)
						log.debug(insert_statement)
						execute_statement(insert_statement, db_to_transform)

						#drop table after done copying
						drop_table = "DROP TABLE IF EXISTS %s_old;" % table
						execute_statement(drop_table, db_to_transform)					
						
						break
						
					else:
						
						add_new_field_statement = "ALTER TABLE %s ADD %s;" %(table, clean_fieldname)
						
						log.debug("Add new field name %s" % field_name)
						log.debug(add_new_field_statement)
						
						execute_statement(add_new_field_statement, db_to_transform)
						
						
			
			#table delete fields names
			if deleted_fields or modified_fields:
				#drop table if exist
				drop_table = "DROP TABLE IF EXISTS %s_old;" % table
				execute_statement(drop_table, db_to_transform)
				#rename table
				rename_table = "ALTER TABLE %s RENAME TO %s_old;" %(table, table)
				execute_statement(rename_table, db_to_transform)
				
				#re-create table with new schema
				sql_stmt = new_table_results[table]["sql_stmt"]
				execute_statement(sql_stmt, db_to_transform)
				
				#reinsert data to the new table
				new_fieldnames = ", ".join(new_print.keys())
				insert_statement = "INSERT INTO %s (%s) SELECT %s FROM %s_old;" % (table, new_fieldnames, new_fieldnames, table)
				execute_statement(insert_statement, db_to_transform)
				
				#drop table after done copying
				drop_table = "DROP TABLE IF EXISTS %s_old;" % table
				execute_statement(drop_table, db_to_transform)


#process new indices
def process_new_index(_results, new_index_results, db_to_transform):
	if _results:

		log.debug("- Process new indexes")

		for result in _results:
			sql_stmt = new_index_results[result]
			
			log.debug("SQL STATEMENT %s" % sql_stmt)
			
			execute_statement(sql_stmt, db_to_transform)

#process deleted indexes	
def process_deleted_index(_results, db_to_transform):
	
	if _results:

		log.debug("- Process deleted indexes")

		for result in _results:
			sql_stmt = "DROP INDEX %s;" % result
			execute_statement(sql_stmt, db_to_transform)



def compare_lookup_row_data(old_db, old_table, new_db, new_table):

	stmt_old_table = "Select * from %s;" % old_table 
	old_results = execute_statement(stmt_old_table, old_db, get_results=True)
	
	stmt_new_table = "Select * from %s;" % new_table 
	new_results = execute_statement(stmt_new_table, new_db, get_results=True)
	
	counter = 0
	
	results = "EQUALS"
	
	if len(old_results) != len(new_results):
		results = "NOT EQUALS"
	
	while counter < len(old_results) and counter < len(new_results):
		if old_results[counter] != new_results[counter]:
			results = "NOT EQUALS"
		
		counter = counter + 1
	
	return results

#check check tables 
def _check_table_schemas(_results, new_table_results, db_to_transform):
	
	schema_status = "EQUALS"

	if _results:
		
		for table,val in _results.items():
			
			old_foreign_keys = _results[table]["old_print"]["foreign_keys"]
			new_foreign_keys = _results[table]["new_print"]["foreign_keys"]
			old_print_columns =  _results[table]["old_print"]["columns"]
			new_print_columns =  _results[table]["new_print"]["columns"]
			
						
			#check foreign keys and columns schema list if the same content	
			
			if set(old_foreign_keys) != set(new_foreign_keys) or set(old_print_columns) != set(new_print_columns):
				log.debug("Need to rerun the whole process, tables schema not the same! Try to run the script again")
				schema_status = "NOT EQUALS"	
			
	return schema_status

def _check_table_indexes(new_indexes, deleted_indexes, modified_indexes):
	indexes_status = "EQUALS"

	if new_indexes or deleted_indexes or modified_indexes:
		indexes_status = "NOT EQUALS"	
	
	return  indexes_status

def make_sure_tables_rows_data_are_equal(db_filename_old, db_filename_new):
	
	log.debug("CHECK TABLES ROWS DATA ARE EQUAL BETWEEN TWO DB'S")
	
	stmt = "SELECT name FROM sqlite_master WHERE type='table' and name != 'sqlite_sequence' and name like 'L_%';"
	
	tables_old = execute_statement(stmt, db_filename_old, get_results=True)
	tables_new =  execute_statement(stmt, db_filename_new, get_results=True)
	
	
	tables_old = sorted(list(zip(*tables_old))[0])
	tables_new = sorted(list(zip(*tables_new))[0])
	
	log.debug("old db lookup table --> %s" % tables_old)
	log.debug("new db lookup table --> %s" % tables_new)
	
	counter = 0
	
	if len(tables_old) != len(tables_new):
		log.debug("The return  tables list length are not equal")
		
		return
	
	tables_not_equal_rows = []
	
	
	while counter < len(tables_old) and counter < len(tables_new):
		if tables_old[counter] != tables_new[counter]:
			
			log.debug("The return  tables list indexes data are not equal")
			
			tables_not_equal_rows.append(tables_old[counter])
		
		result = compare_lookup_row_data(db_filename_old, tables_old[counter], db_filename_new, tables_new[counter])
		
		
		counter = counter + 1
	
	if tables_not_equal_rows:
		for table in tables_not_equal_rows:
			#truncate old table datas
			statement = "DELETE FROM %s;" % table
			execute_statement(statement, db_filename_old)
			
			#reinsert the new data
			stmt = """
			ATTACH DATABASE '%s' AS old_db;
			ATTACH DATABASE '%s' AS new_db;
			INSERT INTO old_db.%s SELECT * FROM new_db.%s;
			""" %(db_filename_old, db_filename_new, table, table)
			
			log.debug("sql statement %s" % stmt)
			
			execute_statement(stmt, db_filename_old, multiple_statement=True)
			
	
def transform(db_filename_old, db_filename_new):

	result_first = db_structure(db_filename_old)
	result_second = db_structure(db_filename_new)
	
	log.debug("\n---- Transform result for %s" % db_filename_old)
	log.debug(json.dumps(result_first, indent=4))
	
	
	log.debug("---- Transform result %s" % db_filename_new)
	log.debug(json.dumps(result_second, indent=4))
	
	#compare two db's tables 
	new_tables, deleted_tables, modified_tables, same_tables = compare_results(result_first["TABLES"], result_second["TABLES"])
	
	log.debug("--new tables--")
	log.debug(json.dumps(new_tables, indent=4))
	
	log.debug("--deleted tables--")
	log.debug(json.dumps(deleted_tables, indent=4))
	
	log.debug("--modified tables--")
	log.debug(json.dumps(modified_tables, indent=4))

	log.debug("--same tables--")
	log.debug(json.dumps(same_tables, indent=2))
	
	#tables processing schemas
	process_new_table(new_tables, result_second["TABLES"], db_filename_old, db_filename_new)
	process_deleted_table(deleted_tables, db_filename_old)
	process_modified_table(modified_tables, result_second["TABLES"], db_filename_old)
	
	#make sure all lookup tables row data are equal
	make_sure_tables_rows_data_are_equal(db_filename_old, db_filename_new)
	
	########################################################################################

	log.debug("#"*30)
	
	#to compare the indexes, reinitialized again the db structure
	
	log.debug("--> Process new/deleted indexes")
	
	result_first = db_structure(db_filename_old)
	result_second = db_structure(db_filename_new)	

	new_indexes, deleted_indexes, modified_indexes, same_indexes = compare_results(result_first["INDEXES"], result_second["INDEXES"])
	
	#indexes processing
	process_new_index(new_indexes, result_second["INDEXES"], db_filename_old)
	process_deleted_index(deleted_indexes, db_filename_old)
	
	########################################################################################
	
	log.debug("#"*30)
	
	#recheck table schemas and indexes via foreign keys and columns schemas; rerun again the db structure comparison
	
	result_first = db_structure(db_filename_old)
	result_second = db_structure(db_filename_new)	
	
	#check tables schemas
	new_tables, deleted_tables, modified_tables, same_tables = compare_results(result_first["TABLES"], result_second["TABLES"])
	schemas_status = _check_table_schemas(modified_tables, result_second["TABLES"], db_filename_old)
	
	#check tables indexes
	new_indexes, deleted_indexes, modified_indexes, same_indexes = compare_results(result_first["INDEXES"], result_second["INDEXES"])
	indexes_status = _check_table_indexes(new_indexes, deleted_indexes, modified_indexes)
		
	return schemas_status,  indexes_status 
		
def parse_args():
	parser = ArgumentParser()

	parser.add_argument('--old_db',
					dest='old_db',
					type=str,
					required=True,
					help='Path for old database')

	parser.add_argument('--new_db',
					dest='new_db',
					type=str,
					required=True,
					help='Path for new database')
	
	return parser.parse_args()

def main():
	args = parse_args()
	
	old_db_file = args.old_db
	new_db_file = args.new_db
	
	schema_status,index_status = transform(old_db_file, new_db_file)
	
	print("==> CHECKING SCHEMAS AND INDEXES BETWEEN TWO DATABASES\n")
	print("\t TABLE SCHEMAS ARE %s" % schema_status)
	print("\t TABLE INDEXES ARE %s\n" % index_status)
	

if __name__ == "__main__":
	main()
	
#python thisfile.py --old_db=<path-to-old-db> --new_db=<path-to-new-db>	